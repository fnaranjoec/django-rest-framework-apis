# Generated by Django 2.1.2 on 2019-08-16 21:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apirosamistica', '0019_auto_20190814_2005'),
    ]

    operations = [
        migrations.AddField(
            model_name='spventasdiarias',
            name='CIdCliente',
            field=models.CharField(default='', max_length=36, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='spventasdiarias',
            name='CNomCliente',
            field=models.CharField(default='', max_length=255),
        ),
        migrations.AlterUniqueTogether(
            name='spventasdiarias',
            unique_together={('CIdCliente', 'CNumFacturaCli')},
        ),
    ]
